const data = require("../data.js");
const errorHandler = require("../errorHandler.js");

function filterFive (data) {
// Tempat penampungan hasil
const result = [];

data.forEach((data) => {
    if (data.registered < "2016-10-11T12:25:56 -07:00" && data.isActive) {
        result.push(data);
    }
    
})

return (result.length < 1) ? errorHandler() : result;
}

module.exports = filterFive(data)